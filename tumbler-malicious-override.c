/* vim: set sts=4 sw=4 et :
 *
 * A small library that can be loaded using LD_PRELOAD to do malicious things 
 * to test whether apparmor works
 */

#include "function-malicious-override.h"

#define _GNU_SOURCE
#include <dlfcn.h>
#include <sys/types.h>

void*
tumbler_provider_factory_get_default (void)
{
    void* (*orig_f)(void) = dlsym (RTLD_NEXT, "tumbler_provider_factory_get_default");

    do_malicious_stuff ();

    return orig_f ();
}
