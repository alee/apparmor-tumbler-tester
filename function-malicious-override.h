/* vim: set sts=4 sw=4 et :
 *
 * A small library that can be loaded using LD_PRELOAD to do malicious things 
 * to test whether apparmor works
 */

#ifndef FUNCTION_MALICIOUS_OVERRIDE_H
#define FUNCTION_MALICIOUS_OVERRIDE_H

void do_malicious_stuff (void);

#endif
