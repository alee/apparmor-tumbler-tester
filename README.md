Test utilities for apparmor-tumbler tests.

# Build

Run autotooling to create configure script:

```
$ autoreconf -i
```

Follow standard configuration and build:

```
$ ./configure
$ make
```
